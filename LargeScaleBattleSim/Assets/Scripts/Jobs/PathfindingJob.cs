﻿using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using UnityEngine;
using UnityEngine.Jobs;

[BurstCompile]
public struct PathfindingJob : IJobParallelForTransform {

    public float movementSpeed;
    public Vector3 targetPos;
    public float deltaTime;

    public void Execute(int index, TransformAccess transform)
    {
        Vector3 pos = transform.position;

        Vector3 direction = new Vector3(targetPos.x - pos.x, targetPos.y - pos.y, targetPos.z - pos.z);

        direction = direction.normalized * movementSpeed * deltaTime;

        transform.position += direction;
    }
}
