﻿using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

public class MovementJobGameManager : MonoBehaviour {

    public static MovementJobGameManager Instance;


    public float topBound = 16.5f;
    public float bottomBound = -13.5f;
    public float leftBound = -23.5f;
    public float rightBound = 23.5f;

    public GameObject unitPrefab;
    public float unitSpeed;

    public int unitCount = 1;
    public int unitIncrement = 1;

    int count;


    private TransformAccessArray _transforms;
    private MovementJob _moveJob;
    private JobHandle _moveHandle;

    public void OnDisable()
    {
        _moveHandle.Complete();
        _transforms.Dispose();
    }

    public void Start()
    {
        _transforms = new TransformAccessArray(0, -1);

        AddUnits(unitCount);
    }

    public void Update()
    {
        _moveHandle.Complete();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddUnits(unitIncrement);
        }

        _moveJob = new MovementJob()
        {
            moveSpeed = unitSpeed,
            topBound = topBound,
            bottomBound = bottomBound,
            deltaTime = Time.deltaTime
        };

        _moveHandle = _moveJob.Schedule(_transforms);

        JobHandle.ScheduleBatchedJobs();
    }

    private void AddUnits(int amount)
    {
        _moveHandle.Complete();

        _transforms.capacity = _transforms.length + amount;

        for (int i = 0; i < amount; i++)
        {
            float xVal = Random.Range(leftBound, rightBound);
            float zVal = Random.Range(0f, 10f);

            Vector3 pos = new Vector3(xVal, 0f, zVal + topBound);
            Quaternion rot = Quaternion.Euler(0f, 0f, 0f);

            var obj = Instantiate(unitPrefab, pos, rot) as GameObject;

            _transforms.Add(obj.transform);
        }

        count += amount;
    }
}
