﻿using System.Collections;
using System.Collections.Generic;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

public class PathfindingJobGameManager : MonoBehaviour
{

    public static PathfindingJobGameManager Instance;

    [SerializeField] private GameObject _targetObject;
    [SerializeField] private GameObject _unitPrefab;

    [SerializeField] private float _targetSpeed;
    [SerializeField] private float _unitSpeed;
    [SerializeField] private int _unitCount;
    [SerializeField] private int _unitIncrement;

    int count;


    private TransformAccessArray _unitTransforms;
    private PathfindingJob _pathfindingJob;
    private JobHandle _pathfindingHandle;

    public void OnDisable()
    {
        _pathfindingHandle.Complete();
        _unitTransforms.Dispose();
    }

    public void Start()
    {
        _unitTransforms = new TransformAccessArray(0, -1);

        AddUnits(_unitCount);
    }

    public void Update()
    {
        _pathfindingHandle.Complete();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            AddUnits(_unitIncrement);
        }

        Vector3 moveDelta = new Vector3();
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveDelta -= new Vector3(_targetSpeed, 0, 0);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveDelta += new Vector3(_targetSpeed, 0, 0);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            moveDelta += new Vector3(0, 0, _targetSpeed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            moveDelta -= new Vector3(0, 0, _targetSpeed);
        }

        _targetObject.transform.position += (moveDelta * Time.deltaTime);

        _pathfindingJob = new PathfindingJob()
        {
            movementSpeed = _unitSpeed,
            targetPos = _targetObject.transform.position,
            deltaTime = Time.deltaTime
        };

        _pathfindingHandle = _pathfindingJob.Schedule(_unitTransforms);

        JobHandle.ScheduleBatchedJobs();
    }

    private void AddUnits(int amount)
    {
        _pathfindingHandle.Complete();

        _unitTransforms.capacity = _unitTransforms.length + amount;

        for (int i = 0; i < amount; i++)
        {
            float xVal = Random.Range(-20f, 20f);
            float zVal = Random.Range(-20f, 20f);

            Vector3 pos = new Vector3(xVal, 0, zVal);
            Quaternion rot = Quaternion.Euler(0, 0, 0);

            var obj = Instantiate(_unitPrefab, pos, rot) as GameObject;

            _unitTransforms.Add(obj.transform);
        }

        count += amount;
    }
}
